Rails.application.routes.draw do
 
  root 'static_pages#home'
  get '/reviews',         to: 'static_pages#reviews'
  get '/about',           to: 'static_pages#about'
  get '/contact',         to: 'messages#new', as: 'new_message'
  post '/contact',        to: 'messages#create', as: 'create_message'
  get '/purchase',        to: 'static_pages#purchase'
  
  get '/whateverman',    to: 'static_pages#secret'
end
