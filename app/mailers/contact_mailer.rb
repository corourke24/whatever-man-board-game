class ContactMailer < ApplicationMailer
    def contact_me(message)
        @content = message.content

        mail to: "whatevermanthegame@gmail.com", from: message.email
    end
end
